
var LuGame = (function(){
'use strict';

/************************/
var TileManager = (function(){
	
	function Tile(pos){
		this.id=pos;	//might not need an id
		this.isStart = false;	//tile is a start location for newly 'borned'
		this.isEnd = false;
		this.canDragFrom=false;	//dynamically set property by game rules- determines a piece may move from tile
		this.canDragTo=false; 	//dynamically set property by game rules - piece may move to tile
	}

	Tile.prototype.setStatus=function(stat,value){
		this[stat]=value;
	};

	return{
		createBatch:function (num){
			var temp = [];
			for(var count=0;count<num;count++){
				temp[count] = new Tile(count);
			}
			return temp;
		},
		create:function(id){
			return new Tile(id);
		}
	};
})();



/***********************/


//////////////////////////

function LuGame(){
	this.commonTiles = [];
	this.baseTiles = [[], [], [],[]];	
	this.baseMappings = [[], [], [], []];	//combination of common tile and base-specific tile for each base
	this.tileStructure={
		top:[],
		right:[],
		bottom:[],
		left:[]
	};
	this.players = [];	//profile for each play
	this.Bases = [];		//profile for each corner/home on board
	
}

LuGame.prototype.newGame = function(options){
	this.players = options.players;
	this.setupBoard();
};

LuGame.prototype.setupBoard = function(){
	this.setupTiles();
	this.setupBases();
	this.mapBasetoToTiles();
	this.setupStructure();
};

LuGame.prototype.mapBasetoToTiles = function(){

	this.Bases.forEach(function(base,baseId){
		base.mappings.forEach(function(mapping){
			for(var i=mapping.s;i<=mapping.e;i++){
				this.baseMappings[baseId].push(this.commonTiles[i]);	//for base[i], add subset of tiles from 'common'
			}
			//now add home stretch
			this.baseTiles[baseId].forEach(function(baseTile){
				this.baseMappings[baseId].push(baseTile);
			});
		}.bind(this));
	}.bind(this));
};

/*** physical board structure layout, used to construct HTML ***/
LuGame.prototype.setupStructure=function(){
	this.tileStructure.top = [
		[this.commonTiles[11],this.commonTiles[12],this.commonTiles[13]],
		[this.commonTiles[10],this.baseTiles[1][0],this.commonTiles[14]],
		[this.commonTiles[9],this.baseTiles[1][1],this.commonTiles[15]],
		[this.commonTiles[8],this.baseTiles[1][2],this.commonTiles[16]],
		[this.commonTiles[7],this.baseTiles[1][3],this.commonTiles[17]],
		[this.commonTiles[6],this.baseTiles[1][4],this.commonTiles[18]]
	];

	this.tileStructure.bottom = [
		[this.commonTiles[44],this.baseTiles[3][4],this.commonTiles[32]],
		[this.commonTiles[43],this.baseTiles[3][3],this.commonTiles[33]],
		[this.commonTiles[42],this.baseTiles[3][2],this.commonTiles[34]],
		[this.commonTiles[41],this.baseTiles[3][1],this.commonTiles[35]],
		[this.commonTiles[40],this.baseTiles[3][0],this.commonTiles[36]],
		[this.commonTiles[39],this.commonTiles[38],this.commonTiles[37]]
	];

	this.tileStructure.left=[
		[this.commonTiles[0],this.commonTiles[1],this.commonTiles[2],this.commonTiles[3],this.commonTiles[4],this.commonTiles[5]],
		[this.commonTiles[51],this.baseTiles[0],this.baseTiles[1],this.baseTiles[2],this.baseTiles[3],this.baseTiles[4]],
		[this.commonTiles[50],this.commonTiles[49],this.commonTiles[48],this.commonTiles[47],this.commonTiles[46],this.commonTiles[45]]		
	];

	this.tileStructure.right=[
		[this.commonTiles[19],this.commonTiles[20],this.commonTiles[21],this.commonTiles[22],this.commonTiles[23],this.commonTiles[24]],
		[this.baseTiles[4],this.baseTiles[3],this.baseTiles[2],this.baseTiles[1],this.baseTiles[0],this.commonTiles[25]],
		[this.commonTiles[31],this.commonTiles[30],this.commonTiles[29],this.commonTiles[28],this.commonTiles[27],this.commonTiles[26]]		
	];
}

/***** create physical tiles to be traversed by pieces ******/
LuGame.prototype.setupTiles = function(){
	this.commonTiles = TileManager.createBatch(52); //common tiles around the board
	
	//create five 'home stretch tiles' for each base
	this.baseTiles.forEach(function(baseTileArr,idx){
		baseTileArr = TileManager.createBatch(5);	//individual lane on the way home 
		baseTileArr.push(TileManager.create(100+idx).setStatus('isEnd',true));	//final destination - 4 pieces in this tile ends game
	});
};

LuGame.prototype.setupBases = function(){
	this.Bases = [
	{
		active:true, //first two slots active by default
		mappings:[{'s':1,'e':51}],
		player:{},
		pieces:[]
	},{
		active:true,
		mappings:[{'s':14,'e':51},{'s':0,'e':12}],
		player:{},
		pieces:[]
	},{
		active:false,
		mappings:[{'s':27,'e':51},{'s':0,'e':25}],
		player:{},
		pieces:[]
	},{
		active:false,
		mappings:[{'s':40,'e':51},{'s':14,'e':38}],
		player:{},
		pieces:[]
	}];
};

return new LuGame();

})();